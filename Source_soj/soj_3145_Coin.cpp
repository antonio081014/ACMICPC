#include<stdio.h>

int main()
{
	int count;
	long long i,j;
	scanf("%d", count);
	while(count-->0)
	{
		scanf("%lld %lld", &i,&j);
		printf("%lld\n", (i-1)*(j-1)-1);
	}
	return 0;
}
