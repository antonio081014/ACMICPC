#include<stdio.h>

int main()
{
	int count;
	int n;
	scanf("%d", &count);
	while(count-->0)
	{
		scanf("%d",&n);
		if(n<=10)
		{
			printf("%.2f\n", 1.00);
		}
		else
		{
			printf("%.2f\n",1+(n-10)*0.15);
		}
	}
	return 0;
}
