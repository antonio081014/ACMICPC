/**
 * Problem ID: 1820.
 */

/**
 * @author antonio081014
 * @since Dec 1, 2011, 11:53:02 AM
 */
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] strs = br.readLine().split(" ");
		int n = Integer.parseInt(strs[0]);
		int k = Integer.parseInt(strs[1]);
		if (k > n)
			k = n;
		System.out.println((2 * n + k - 1) / k);
	}

}

