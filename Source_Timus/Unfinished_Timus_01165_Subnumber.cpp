//Fri Apr  9 03:05:32 CDT 2010
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

int main(int argc, char* argv[])
{
	freopen("input.in", "r", stdin);
	freopen("output.out", "w", stdout);
	string temp = "1234567890";
	string tmp = temp;
	size_t found;
	string test;
	cin >> test;
	found=temp.find_first_of(test);
	while(found != string::npos)
	{
		temp += tmp;
		found = temp.find_first_of(test);
	}
	cout << (long long) found << endl;
	fclose(stdin);
	fclose(stdout);
	return 0;
}
