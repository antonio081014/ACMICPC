import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Problem ID: 1705.
 */

/**
 * @author antonio081014
 * @since Dec 1, 2011, 11:44:50 AM
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());
		if (N < 1)
			return;
		if (N < 5) {
			System.out.println("few");
			return;
		}
		if (N < 10) {
			System.out.println("several");
			return;
		}
		if (N < 20) {
			System.out.println("pack");
			return;
		}
		if (N < 50) {
			System.out.println("lots");
			return;
		}
		if (N < 100) {
			System.out.println("horde");
			return;
		}
		if (N < 250) {
			System.out.println("throng");
			return;
		}
		if (N < 500) {
			System.out.println("swarm");
			return;
		}
		if (N < 1000) {
			System.out.println("zounds");
			return;
		}
		System.out.println("legion");
	}
}

