//Mon Apr 26 22:51:25 CDT 2010
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

int main(int argc, char* argv[])
{
//	freopen("input.in", "r", stdin);
//	freopen("output.out", "w", stdout);
	long long d, e, f, dp, ep, h;
	while (cin >> d >> e >> f >> dp >> ep >> h)
	{
		long long adepth = 0;
		dp--;
		ep--;
		while (dp != ep)
		{
			dp >>= 1;
			ep >>= 1;
			adepth++;
		}
		if (adepth > max(d, e))
		{
			if (adepth + adepth - d - e > h)
				cout << "NO" << endl;
			else
				cout << "YES" << endl;
		}
		else
		{
			if (abs((double) d - e) > h)
				cout << "NO" << endl;
			else
				cout << "YES" << endl;
		}
	}
//	fclose(stdin);
//	fclose(stdout);
	return 0;
}
