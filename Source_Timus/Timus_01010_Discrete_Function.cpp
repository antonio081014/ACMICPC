//Fri Apr  9 00:58:12 CDT 2010
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

long long longabs(long long a)
{
	if(a > 0) return a;
	return -1*a;
}

int main(int argc, char* argv[])
{
	freopen("input.in", "r", stdin);
	freopen("output.out", "w", stdout);
	int N;
	while(cin >> N)
	{
		vector<long long> v(N, 0);
		for(int i=0; i<N; i++)
			cin >> v[i];
		int idx = 0;
		for(int i=0; i+1<N; i++)
		{
			if(longabs(v[i]-v[i+1]) > longabs(v[idx]-v[idx+1]))
				idx = i;
		}
		cout << idx+1 << " " << idx+2 << endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
