//Thu Apr  8 18:38:09 CDT 2010
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

#define		eps		1e-9

int main(int argc, char* argv[])
{
	freopen("input.in", "r", stdin);
	freopen("output.out", "w", stdout);
	long double P, Q;
	cin >> P >> Q;
	for(int i=1; ; i++)
	{
		if((int)(P*i/100+eps) < (int)(Q*i/100-eps))
		{
			cout << i << endl;
			break;
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
