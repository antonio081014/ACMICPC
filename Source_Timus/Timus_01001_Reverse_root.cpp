//Wed Mar 31 23:25:23 CDT 2010
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

int main(int argc, char* argv[])
{
	freopen("input.in", "r", stdin);
	freopen("output.out", "w", stdout);
	vector<double> root;
	long long number;
	while (cin >> number)
	{
		double tmp = sqrt(1.0 * number);
		root.push_back(tmp);
	}
	reverse(root.begin(), root.end());
	for (int i = 0; i < (int) root.size(); i++)
	{
		cout.precision(4);
		cout << fixed << root[i] << endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
