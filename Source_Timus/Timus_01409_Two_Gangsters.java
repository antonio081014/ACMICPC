/**
 * Ad-hoc.
 * The sum of total shooting is a + b - 1.
 * Then, the number of not being shot is b-1, a-1.
 */

/**
 * @author antonio081014
 * @since Dec 1, 2011, 11:38:58 AM
 */
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] strs = br.readLine().split(" ");
		int a = Integer.parseInt(strs[0]);
		int b = Integer.parseInt(strs[1]);
		System.out.println((b - 1) + " " + (a - 1));
	}

}

