#!/bin/sh

if [ $# -lt 2 ]
then 
	echo "There should be more than two arguments needed."
	exit 0
fi

for i in "$@"
do
	STR=$STR\_$i
done

echo -n "//" >> hit$STR\.cpp
date >> hit$STR\.cpp
cat Temple >> hit$STR\.cpp
mv hit$STR\.cpp HIT_Judge/
