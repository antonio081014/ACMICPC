#!/bin/sh

if [ $# -lt 2 ]
then 
	echo "There should be more than two arguments needed."
	exit 0
fi

for i in "$@"
do
	STR=$STR\_$i
done

echo -n "//" >> coj$STR\.java
date >> poj$STR\.java
#cat Temple >> poj$STR\.java
#mv poj$STR\.cpp POJ_Judge/
