//Tue Mar  9 03:58:55 CST 2010
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

vector<double> v;

void init(int N) {
	v.clear();
	v.resize(N);
	for (int i = 0; i < N; i++)
		cin >> v[i];
}

int main(int argc, char* argv[]) {
	//	freopen("input.txt", "r", stdin);
	//	freopen("output.txt", "w", stdout);
	int T;
	cin >> T;
	while (T--) {
		int N;
		cin >> N;
		init(N);
		sort(v.begin(), v.end());
		if (N % 2 == 1)
			printf("%.3f\n", v[(N - 1) / 2]);
		else
			printf("%.3f\n", (v[N / 2 - 1] + v[N / 2]) / 2);
	}
	//	fclose(stdin);
	//	fclose(stdout);
	return 0;
}

