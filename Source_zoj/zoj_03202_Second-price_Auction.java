import java.util.Scanner;

/**
 * 
 */

/**
 * @author antonio081014
 * @since Feb 24, 2012, 5:14:49 PM
 */
public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {
        Main main = new Main();
        main.solve();
        // The following statement will result with runtime error.
        // System.exit(0);
    }

    public void solve() {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        while ((T--) > 0) {
            int N = sc.nextInt();
            int idx = -1;
            int max1 = 0;
            int max2 = 0;
            for (int i = 0; i < N; i++) {
                int a = sc.nextInt();
                if (a > max1) {
                    idx = i + 1;
                    max2 = max1;
                    max1 = a;
                }
                else if (a > max2) {
                    max2 = a;
                }
            }
            System.out.println("" + idx + " " + max2);
        }
    }
}

