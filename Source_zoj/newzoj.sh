#!/bin/sh

if [ $# -lt 2 ]
then 
	echo "There should be more than two arguments needed."
	exit 0
fi

for i in "$@"
do
	STR=$STR\_$i
done

echo -n "//" >> zoj$STR\.cpp
date >> zoj$STR\.cpp
cat Temple >> zoj$STR\.cpp
mv zoj$STR\.cpp Judge/
#gedit zoj$STR\.cpp &
