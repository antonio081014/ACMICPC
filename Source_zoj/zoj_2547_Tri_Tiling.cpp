//Fri Jul  9 12:46:14 CDT 2010
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#define MMAX 31
using namespace std;

int Tiling(vector<int> &v, int N)
{
	if (N == 0)
		return v[0] = 1;
	if (N == 2)
		return v[2] = 3;
	if (N % 2 == 1)
		return v[N] = 0;
	if (v[N] != -1)
		return v[N];
	else
		return 4 * Tiling(v, N - 2) - Tiling(v, N - 4);
}

int main(int argc, const char* argv[])
{
//	freopen("input.in", "r", stdin);
//	freopen("output.out", "w", stdout);
	vector<int> v;
	for (int i = 0; i < MMAX; i++)
	{
		v.push_back(-1);
	}
	for (int i = 0; i < MMAX; i++)
		v[i] = Tiling(v, i);
	int N;
	while (cin >> N && N != -1)
		cout << v[N] << endl;
//	fclose(stdin);
//	fclose(stdout);
	return 0;
}
/*
 * 2663时得考虑几个地方,
 * 1.做了2506后就会明白0时是1而不是0的原因,无即是有1,
 * 2.奇数时不可能满足，因为砖都是2的面积,n为奇数时面积为奇数，
 * 只要考虑偶数,
 * 3.很容易看到 有 f(n-2) * 3,
 * 但是n也可能依靠n-4，
 * 很容易想清楚要满足4的话,只要2中组合,但是到这里了就很容易遗漏，依靠n-6，n-8,
 * 也就是说可以8组合，10组合，乃至全部偶数一起组合，这样的话，3排有一排必须全是横的，
 * 另外2个了，必须从外到内不能独立出去一个,这样每种情况都是2。
 *
 * 所以公式是，f(n)= 3 *f(n-2) +2( f(0)+ f(2) +...+f(n-4))
 * 解递推方程得，f(n)= 4 * f(n-2) -f(n-4)
 *  */
