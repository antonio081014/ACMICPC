//Mon Feb 21 20:13:38 CST 2011
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

int main(int argc, char* argv[]) {
	//freopen("input.in", "r", stdin);
	//freopen("output.out", "w", stdout);
	int t;
	while (cin >> t && t) {
		string s1;
		cin >> s1;
		string s2(s1);
		int rows = s1.size() / t;
		for (int i = 0; i < (int) s1.size(); i++) {
			int m = i / t;
			//check which line it's at, odd line or even line.
			int n = (m % 2 == 0 ? i % t : t - 1 - i % t);
			s2[n * rows + m] = s1[i];
		}
		cout << s2 << endl;
	}
	//fclose(stdin);
	//fclose(stdout);
	return 0;
}
