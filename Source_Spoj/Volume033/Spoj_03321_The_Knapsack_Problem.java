//Tue Oct  4 14:22:14 PDT 2011
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			InputStreamReader in = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(in);
			String strLine = br.readLine();
			int W = Integer.parseInt(strLine.split(" ")[0]);
			int N = Integer.parseInt(strLine.split(" ")[1]);
			int[] weight = new int[N];
			int[] values = new int[N];
			for (int i = 0; i < N; i++) {
				strLine = br.readLine();
				weight[i] = Integer.parseInt(strLine.split(" ")[0]);
				values[i] = Integer.parseInt(strLine.split(" ")[1]);
			}
			int[][] dp = new int[N + 1][W + 1];
			for (int i = 0; i <= N; i++) {
				for (int j = 0; j <= W; j++)
					dp[i][j] = 0;
			}
			for (int i = 1; i <= N; i++) {
				dp[i][0] = 0;
				dp[i][weight[i - 1]] = values[i - 1];
			}

			for (int i = 1; i <= N; i++) {
				for (int j = 1; j <= W; j++) {
					dp[i][j] = Math.max(dp[i][j], dp[i - 1][j]);
					if (j >= weight[i - 1])
						dp[i][j] = Math.max(dp[i][j], values[i - 1]
								+ dp[i - 1][j - weight[i - 1]]);
				}
			}
			// System.out.println(Integer.MIN_VALUE);
			System.out.println(dp[N][W]);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
