/*
 * helptest.cpp
 *
 *  Created on: Apr 19, 2010
 *      Author: yi
 */
#include<iostream>
#include<string>
#include<cstdio>

using namespace std;

int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	for(int j=18; j<=65; j++)
	{
		cout << "<table><tbody>" << endl;
		for (int i = 1; i <= 100; i++)
		{
			if (i % 10 == 1)
				cout << "<tr>";
			cout << "<td>" << 100*j + i << "</td>";
			if (i % 10 == 0)
				cout << "</tr>" << endl;
		}
		cout << "</tbody></table>" << endl << endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
