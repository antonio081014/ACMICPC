//Tue Oct  4 19:00:48 PDT 2011
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

/**
 * 
 */

/**
 * @author antonio081014
 * @date Oct 4, 2011
 * @time 3:13:01 PM
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			InputStreamReader in = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(in);
			String[] strLine;
			while ((strLine = br.readLine().split(" ")) != null) {
				int[] nums = new int[strLine.length];
				for (int i = 0; i < nums.length; i++)
					nums[i] = Integer.parseInt(strLine[i]);
				nums = mergeSort(nums);
				System.out.print(nums[0]);
				for (int i = 1; i < nums.length; i++) {
					System.out.print(" " + nums[i]);
				}
				System.out.println();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int[] mergeSort(int[] nums) {
		// int[] nums = numss.clone();
		// for (int p = 0; p < nums.length; p++)
		// System.out.print(nums[p] + ", ");
		// System.out.println();
		if (nums.length == 1)
			return nums;
		if (nums.length == 2) {
			if (nums[0] > nums[1]) {
				nums[0] ^= nums[1];
				nums[1] ^= nums[0];
				nums[0] ^= nums[1];
			}
			return nums;
		}

		Random r = new Random();
		int pivot = nums[r.nextInt(nums.length)];
		ArrayList<Integer> num1 = new ArrayList<Integer>();
		ArrayList<Integer> num2 = new ArrayList<Integer>();
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] <= pivot)
				num1.add(nums[i]);
			else
				num2.add(nums[i]);
		}
		int[] numArray1 = new int[num1.size()];
		int[] numArray2 = new int[num2.size()];
		for (int i = 0; i < num1.size(); i++)
			numArray1[i] = num1.get(i);
		for (int i = 0; i < num2.size(); i++)
			numArray2[i] = num2.get(i);

		// Random pivot could make one of the array empty;
		if (numArray1.length > 0)
			numArray1 = mergeSort(numArray1);
		if (numArray2.length > 0)
			numArray2 = mergeSort(numArray2);

		int i = 0;
		int j = 0;
		int k = 0;
		for (; i < numArray1.length && j < numArray2.length;) {
			if (numArray1[i] < numArray2[j]) {
				nums[k++] = numArray1[i++];
			} else {
				nums[k++] = numArray2[j++];
			}
		}
		for (; i < numArray1.length;)
			nums[k++] = numArray1[i++];
		for (; j < numArray2.length;)
			nums[k++] = numArray2[j++];
		// for (int p = 0; p < nums.length; p++)
		// System.out.print(nums[p] + ", ");
		// System.out.println();
		// System.out.println();
		// System.out.println();
		return nums;
	}
}
