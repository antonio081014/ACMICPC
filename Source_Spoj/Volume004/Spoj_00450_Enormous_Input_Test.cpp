//Wed Mar 16 23:27:16 CDT 2011
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
 
using namespace std;
 
int main(int argc, char* argv[])
{
    //freopen("input.in", "r", stdin);
    //freopen("output.out", "w", stdout);
    long long int n, k;
    scanf("%lld%lld", &n, &k);
    int count = 0;
    while (n--)
    {
        long long int x;
        scanf("%lld", &x);
        if (x % k == 0)
            count++;
    }
    printf("%d\n", count);
    //fclose(stdin);
    //fclose(stdout);
    return 0;
}
