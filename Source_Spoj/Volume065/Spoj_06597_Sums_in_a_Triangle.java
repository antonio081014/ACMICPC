//Fri Oct 14 20:09:41 PDT 2011
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {
	public static void main(String[] args) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			int N = Integer.parseInt(br.readLine());
			for (int i = 0; i < N; i++) {
				int n = Integer.parseInt(br.readLine().trim());
				int[][] nums = new int[2][n];
				int mmax = 0;
				for (int j = 0; j < n; j++) {
					StringTokenizer stk = new StringTokenizer(br.readLine()
							.trim());
					int len = stk.countTokens();
					for (int k = 0; k < len; k++) {
						int tmp = Integer
								.parseInt(stk.nextElement().toString());
						// System.out.print(tmp + ", ");
						if (k > 0)
							nums[j % 2][k] = Math.max(nums[1 - j % 2][k],
									nums[1 - j % 2][k - 1])
									+ tmp;
						else
							nums[j % 2][k] = nums[1 - j % 2][k] + tmp;
						mmax = Math.max(mmax, nums[j % 2][k]);
						// System.out.print(tmp + ", ");
					}
					// System.out.println();
				}
				System.out.println();
				System.out.println(mmax);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
