//Thu Apr 29 19:17:56 CDT 2010
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

int main(int argc, char* argv[])
{
//	freopen("input.in", "r", stdin);
//	freopen("output.out", "w", stdout);
	int N;
	cin >> N;
	for (int n = 0; n < N; n++)
	{
		string str1, str2, str3;
		cin >> str1 >> str2;
		if (str1.size() > str2.size())
		{
			str3 = str1;
			str1 = str2;
			str2 = str3;
		}
		str3.clear();
		int flag = 0;
		for (int i = 0; i < (int) str1.size(); i++)
		{
			int tmp = str1[i] - '0' + str2[i] - '0' + flag;
			if (tmp > 9)
			{
				flag = 1;
				tmp %= 10;
			}
			else
			{
				flag = 0;
			}
			str3 += '0' + tmp;
		}
		for (int i = (int) str1.size(); i < (int) str2.size(); i++)
		{
			int tmp = str2[i] - '0' + flag;
			if (tmp > 9)
			{
				flag = 1;
				tmp %= 10;
			}
			else
			{
				flag = 0;
			}
			str3 += '0' + tmp;
		}
		if (flag == 1)
			str3 += "1";
		while(str3.size() > 1 && str3[0]=='0')
		{
			str3 = str3.substr(1);
		}
		cout << str3 << endl;
	}
//	fclose(stdin);
//	fclose(stdout);
	return 0;
}
