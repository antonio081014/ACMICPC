//Sat Apr  3 03:24:05 CDT 2010
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

//map<unsigned long, bool> prime;

bool isPrime(unsigned long number)
{
	if (number == 1)
		return false;
	if (number == 2)
		return true;
	for (unsigned long i = 3; i * i <= number; i += 2)
	{
		if (number % i == 0)
			return false;
	}
	return (number % 2 == 0 ? false : true);
}

//Select all the primes smaller than number, make them be false;
void PrimeSieveEratosthenes(unsigned long lowerLimit, unsigned long upperLimit)
{
	bool *prime = new bool[upperLimit + 1];
	memset(prime, 0, sizeof(bool) * (upperLimit + 1));
	//The default value false is the prime.
	//	for (unsigned long i = 2; i <= upperLimit; i++)
	//		prime[i] = false;
	for (unsigned long i = 2; i <= (unsigned long) sqrt(1.0 * upperLimit); i++)
	{
		if (!prime[i])
		{
			if (i >= lowerLimit)
				cout << i << endl;
			for (unsigned long j = i * i; j <= upperLimit; j += i)
				prime[j] = true;
		}
	}
	for (unsigned long i = (unsigned long) sqrt(1.0 * upperLimit) + 1; i
			<= upperLimit; i++)
		if (!prime[i] && i >= lowerLimit)
			cout << i << endl;
	delete[] prime;
}

int main(int argc, char* argv[])
{
	freopen("input.in", "r", stdin);
	freopen("output.out", "w", stdout);

	int N;
	cin >> N;
	while (N--)
	{
		unsigned long a;
		unsigned long b;
		cin >> a >> b;

		PrimeSieveEratosthenes(a, b);
		cout << endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
