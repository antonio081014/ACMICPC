//Sat Oct 15 12:53:57 PDT 2011
import java.io.*;
import java.util.*;
 
public class Main{
        public static void main(String[] args){
                try{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                int N = Integer.parseInt(br.readLine());
                for(int i=0; i<N; i++){
                        String[] nums = br.readLine().split(" ");
                        if(Integer.parseInt(nums[0]) * Integer.parseInt(nums[2]) <= Integer.parseInt(nums[1]))
                                System.out.println("yes");
                        else
                                System.out.println("no");
                        
                }}catch(Exception e){
                        e.printStackTrace();
                }
        }
}
