//Wed Apr  6 13:24:05 CDT 2011
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

string add(string A, string B)
{
    reverse(A.begin(), A.end());
    reverse(B.begin(), B.end());
    string sum = "";
    int flag = 0;
    int i;
    for (i = 0; i < min(A.size(), B.size()); i++)
    {
        int tmp = (A[i] - '0') + (B[i] - '0') + flag;
        sum += '0' + tmp % 10;
        flag = tmp > 9 ? 1 : 0;
    }
    while (i < A.size())
    {
        int tmp = A[i] - '0' + flag;
        sum += '0' + tmp % 10;
        flag = tmp > 9 ? 1 : 0;
        i++;
    }
    while (i < B.size())
    {
        int tmp = B[i] - '0' + flag;
        sum += '0' + tmp % 10;
        flag = tmp > 9 ? 1 : 0;
        i++;
    }
    if (flag > 0)
        sum += '0' + flag;
    reverse(sum.begin(), sum.end());
    return sum;
}

int main(int argc, char* argv[])
{
    string A;
    string B;
    while (cin >> A >> B)
        cout << add(A, B) << endl;
    return 0;
}
