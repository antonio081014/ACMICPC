//Wed Aug 18 15:25:15 CDT 2010
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

int solve(int n)
{
	int ret = 0;
	for (int i = 5; i <= n; i*=5)
	{
		ret += n / i;
	}
	return ret;
}

int main(int argc, char* argv[])
{
//	freopen("input.in", "r", stdin);
//	freopen("output.out", "w", stdout);

	int N;
	cin >> N;
	while(N--)
	{
		int n;
		cin >> n;
		cout << solve(n) << endl;
	}
//	fclose(stdin);
//	fclose(stdout);
	return 0;
}
