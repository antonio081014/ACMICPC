//Wed Mar 16 23:27:16 CDT 2011
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

string solve(vector<vector<int> > &v, int m, string s1, int n, string s2)
{
    string str = "";
    for (int i = 1; i <= m; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            if (s1[i - 1] == s2[j - 1])
            {
                str += s1[i - 1];
                v[i][j] = max(v[i][j], v[i - 1][j - 1] + 1);
            }
            else
            {
                v[i][j] = max(v[i][j], max(v[i - 1][j], v[i][j - 1]));
            }
        }
    }
    return str;
}

void print(vector<vector<int> > &v, int m, string s1, int n, string s2)
{
    cout << v[m][n] << endl;
}

int main(int argc, char* argv[])
{
    freopen("input.in", "r", stdin);
    //freopen("output.out", "w", stdout);
    int T;
    cin >> T;
    int score = 0;
    for (int i = 1; i <= T; i++)
    {
        int m, n;
        string s1, s2;
        cin >> m >> s1;
        cin >> n >> s2;
        vector<vector<int> > count(m + 1, vector<int>(n + 1, 0));
        string str = solve(count, m, s1, n, s2);
        cout << str << endl;
        cout << "case " << i << " ";
        if (count[m][n] > 1)
        {
            score++;
            cout << "Y" << endl;
            print(count, m, s1, n, s2);
        }
        else
        {
            cout << "N" << endl;
        }
    }
    //fclose(stdin);
    //fclose(stdout);
    return 0;
}
