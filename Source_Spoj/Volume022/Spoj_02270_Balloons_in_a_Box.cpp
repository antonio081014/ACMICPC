//Fri Apr  2 11:28:22 CDT 2010
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

#define	PI	3.141592653589793
using namespace std;

class Points
{
public:
	double x;
	double y;
	double z;
};

bool cmp(const Points xx, const Points yy)
{
	if (xx.x > yy.x)
		return true;
	else if (xx.x == yy.x && xx.y > yy.y)
		return true;
	else if (xx.x == yy.x && xx.y == yy.y && xx.z > yy.z)
		return true;
	return false;
}

double dist(Points xx, Points yy)
{
	double distance = (xx.x - yy.x) * (xx.x - yy.x) + (xx.y - yy.y) * (xx.y
			- yy.y) + (xx.z - yy.z) * (xx.z - yy.z);
	return sqrt(distance);
}

int main(int argc, const char* argv[])
{
	freopen("input.in", "r", stdin);
	freopen("output.out", "w", stdout);
	int ncase = 0;
	int N;
	while (cin >> N && N != 0)
	{
		ncase++;
		Points inside;
		Points outside;
		cin >> inside.x >> inside.y >> inside.z;
		cin >> outside.x >> outside.y >> outside.z;
		if (cmp(outside, inside))
		{
			Points temp = inside;
			inside = outside;
			outside = temp;
		}
		vector<Points> v;
		for (int i = 0; i < N; i++)
		{
			Points tmp;
			cin >> tmp.x >> tmp.y >> tmp.z;
			if (cmp(inside, tmp) && cmp(tmp, outside))
			{
				v.push_back(tmp);
			}
		}
		sort(v.begin(), v.end(), cmp);
		vector<double> R;
		double ret = fabs(inside.x - outside.x) * fabs(inside.y - outside.y)
				* fabs(inside.z - outside.z);
		double maxVolumn = 0.0;
//		int count = 0;
		do
		{
//			count++;
//			cout << count;
			R.clear();
			double tmpVolumn = 0.0;
			for (int i = 0; i < (int) v.size(); i++)
			{
				double tmpR = min(fabs(v[i].x - inside.x), fabs(v[i].y
						- inside.y));
				tmpR = min(tmpR, fabs(v[i].z - inside.z));
				tmpR = min(tmpR, fabs(v[i].x - outside.x));
				tmpR = min(tmpR, fabs(v[i].y - outside.y));
				tmpR = min(tmpR, fabs(v[i].z - outside.z));
				for (int j = 0; j < i; j++)
				{
					double distance = dist(v[i], v[j]) - R[j];
					if (distance >= 0.0)
						tmpR = min(tmpR, distance);
					else
						tmpR = 0.0;
				}
				R.push_back(tmpR);
				tmpVolumn += tmpR * tmpR * tmpR;
			}
			maxVolumn = max(tmpVolumn, maxVolumn);
//			cout << ": " << (long long)(ret - 4.0 / 3.0 * maxVolumn) << endl;
		} while (next_permutation(v.begin(), v.end(), cmp));

		ret -= (4.0 / 3.0 * PI * maxVolumn);
		cout << "Box " << ncase << ": ";
//		printf("%.0lf\n", round(ret));
		cout << (long long) round(ret) << endl;
		cout << endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*
 * Problem Link:

2270. Balloons in a Box

Problem code: BALLOON


You must write a program that simulates placing spherical balloons into a rectangular box.
The simulation scenario is as follows. Imagine that you are given a rectangular box and a set of points. Each point represents a position where you might place a balloon. To place a balloon at a point, center it at the point and inflate the balloon until it touches a side of the box or a previously placed balloon. You may not use a point that is outside the box or inside a previously placed balloon. However, you may use the points in any order you like, and you need not use every point. Your objective is to place balloons in the box in an order that maximizes the total volume occupied by the balloons.
You are required to calculate the volume within the box that is not enclosed by the balloons.
Input
The input consists of several test cases. The first line of each test case contains a single integer n that indicates the number of points in the set (1<=n<=6). The second line contains three integers that represent the (x, y, z) integer coordinates of a corner of the box, and the third line contains the (x, y, z) integer coordinates of the opposite corner of the box. The next n lines of the test case contain three integers each, representing the (x, y, z) coordinates of the points in the set. The box has non-zero length in each dimension and its sides are parallel to the coordinate axes.
The input is terminated by the number zero on a line by itself.
Output
For each test case print one line of output consisting of the test case number followed by the volume of the box not occupied by balloons. Round the volume to the nearest integer. Follow the format in the sample output given below.
Place a blank line after the output of each test case.
Example
Input:
2
0 0 0
10 10 10
3 3 3
7 7 7
0

Output:
Box 1: 774
Test Case:
INPUT:
6
808 877 -957 661 329 -738
763 572 -792
687 539 -879
673 795 -811
679 500 -783
717 545 -883
667 629 -825
(Check on ball is in another one).
4
73 256 -38 -175 944 136
-37 332 1
-63 743 64
48 472 -32
35 753 38

2
0 0 0
10 10 10
3 3 3
7 7 7

0
OUTPUT:
Box 1: 16491870

Box 2: 27763968

Box 3: 774

Solution:
Basically, using enumeration is one of the simplest way to implement this problem.
Go through all the situations, find out the maximum of the volume.
For each situation:
1st, Find the shortest length from the origin of the ball to each face(6 faces in total);
2nd, Check and find the shortest length between the current ball with the previous balls. Mainly check if one is in another ball, if so, put the zero as its radius, rather than a positive value.
Source Code:
//Fri Apr  2 11:28:22 CDT 2010
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

#define PI 3.141592653589793
using namespace std;

class Points
{
public:
 double x;
 double y;
 double z;
};

bool cmp(const Points xx, const Points yy)
{
 if (xx.x > yy.x)
  return true;
 else if (xx.x == yy.x && xx.y > yy.y)
  return true;
 else if (xx.x == yy.x && xx.y == yy.y && xx.z > yy.z)
  return true;
 return false;
}

double dist(Points xx, Points yy)
{
 double distance = (xx.x - yy.x) * (xx.x - yy.x) + (xx.y - yy.y) * (xx.y
   - yy.y) + (xx.z - yy.z) * (xx.z - yy.z);
 return sqrt(distance);
}

int main(int argc, const char* argv[])
{
// freopen("input.in", "r", stdin);
// freopen("output.out", "w", stdout);
 int ncase = 0;
 int N;
 while (cin >> N && N != 0)
 {
  ncase++;
  Points inside;
  Points outside;
  cin >> inside.x >> inside.y >> inside.z;
  cin >> outside.x >> outside.y >> outside.z;
  if (cmp(outside, inside))
  {
   Points temp = inside;
   inside = outside;
   outside = temp;
  }
  vector<Points> v;
  for (int i = 0; i < N; i++)
  {
   Points tmp;
   cin >> tmp.x >> tmp.y >> tmp.z;
   if (cmp(inside, tmp) && cmp(tmp, outside))
   {
    v.push_back(tmp);
   }
  }
  sort(v.begin(), v.end(), cmp);
  vector<double> R;
  double ret = fabs(inside.x - outside.x) * fabs(inside.y - outside.y)
    * fabs(inside.z - outside.z);
  double maxVolumn = 0.0;
//  int count = 0;
  do
  {
//   count++;
//   cout << count;
   R.clear();
   double tmpVolumn = 0.0;
   for (int i = 0; i < (int) v.size(); i++)
   {
    double tmpR = min(fabs(v[i].x - inside.x), fabs(v[i].y
      - inside.y));
    tmpR = min(tmpR, fabs(v[i].z - inside.z));
    tmpR = min(tmpR, fabs(v[i].x - outside.x));
    tmpR = min(tmpR, fabs(v[i].y - outside.y));
    tmpR = min(tmpR, fabs(v[i].z - outside.z));
    for (int j = 0; j < i; j++)
    {
     double distance = dist(v[i], v[j]) - R[j];
     if (distance >= 0.0)
      tmpR = min(tmpR, distance);
     else
      tmpR = 0.0;
    }
    R.push_back(tmpR);
    tmpVolumn += tmpR * tmpR * tmpR;
   }
   maxVolumn = max(tmpVolumn, maxVolumn);
//   cout << ": " << (long long)(ret - 4.0 / 3.0 * maxVolumn) << endl;
  } while (next_permutation(v.begin(), v.end(), cmp));

  ret -= (4.0 / 3.0 * PI * maxVolumn);
  cout << "Box " << ncase << ": ";
//  printf("%.0lf\n", round(ret));
  cout << (long long) round(ret) << endl;
  cout << endl;
 }
// fclose(stdin);
// fclose(stdout);
 return 0;
}*/
